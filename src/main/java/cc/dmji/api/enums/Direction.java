package cc.dmji.api.enums;

public enum Direction {
    ASC,
    DESC
}
